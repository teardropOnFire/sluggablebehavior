<?php

	App::import('Sanitize');

	class SluggableBehavior extends ModelBehavior {
		
		var $__settings = array();
		
		function setup(&$Model, $settings = array()) {
			
			$default = array(
				'field' => 'name',
				'separator' => '-',
				'toLower' => true,
				'incremental' => true,
			);
			if (!isset($this->__settings[$Model->alias])) {
				$this->__settings[$Model->alias] = $default;
			}
			$this->__settings[$Model->alias] = am($this->__settings[$Model->alias], ife(is_array($settings), $settings, array()));
		}
		
		function beforeSave(&$Model) {
			
			$return = parent::beforeSave($Model);
			
			if (empty($Model->data[$Model->alias]['slug'])) {
				$slug = $this->generateSlug($Model->data[$Model->alias][$this->__settings[$Model->alias]['field']], $this->__settings[$Model->alias]['separator'], $this->__settings[$Model->alias]['toLower']);
				
				if ($this->__settings[$Model->alias]['incremental']) {
					$conditions = array($Model->alias.'.slug LIKE' => $slug.'%');
					$pk = $Model->primaryKey;
					if (!empty($Model->$pk)) {
						$conditions[$Model->alias.'.'.$Model->primaryKey.' != '] = $Model->$pk;
					}
					$result = $Model->find('all', array('conditions' => $conditions, 'fields' => array($Model->primaryKey, 'slug'), 'recursive' => -1));
					
					$sameUrls = NULL;
					if (!empty($result)) {
						$sameUrls = Set::extract($result, '{n}.'.$Model->alias.'.slug');
					}
					
					if (!empty($sameUrls)) {
						$beginningSlug = $slug;
						$index = 1;
						
						for ($i=1; $i > 0; $i++) {
							if (!in_array($beginningSlug.'-'.$i, $sameUrls)) {
								$slug = $beginningSlug.'-'.$i;
								break;
							}
						}
					}
				}
				
				$Model->data[$Model->alias]['slug'] = $slug;
			}
			return $return;
			
		}
		
		function generateSlug($value, $separator, $toLower) {
			$value = preg_replace('/&/', 'and', $value);
			if ($toLower) {
				$value = low($value);
			}
			return Inflector::slug(Sanitize::paranoid($value,array(' ','-')), $separator);
		}
		
	}

?>
